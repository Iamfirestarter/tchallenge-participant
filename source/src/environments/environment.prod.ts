export const environment = {
    apiBaseUrl: 'http://backend-service-vladimir-test.openshift.devops.t-systems.ru',
    clientBaseUrl: 'http://localhost:4200',
    production: true
};
