export const environment = {
    apiBaseUrl: 'http://backend-route-vladimir-prod.openshift.devops.t-systems.ru',
    clientBaseUrl: 'http://localhost:4200',
    production: true
};
