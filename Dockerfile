FROM node:14

WORKDIR /app

ADD ./source/ /app/


CMD npm run start -- --host 0.0.0.0 --disableHostCheck true